#!/bin/bash

# Get super user 
su

# Update your system 
sudo apt-get update && sudo apt-get dist-upgrade

# Codecs
sudo apt-get install ubuntu-restricted-extras
sudo apt-get install ffmpeg
sudo apt-get install libavcodec-extra
sudo apt-get install libdvd-pkg
# Flash
sudo apt-get install flashplugin-installer pepperflashplugin-nonfree

# Clean-up System
sudo apt-get purge epiphany-browser epiphany-browser-data #browser
sudo apt-get purge midori-granite #browser
sudo apt-get purge noise
sudo apt-get purge scratch-text-editor #text-editor
sudo apt-get purge modemmanager
sudo apt-get purge geary #email
sudo apt-get purge pantheon-mail #email
sudo apt-get purge pantheon-terminal #terminal
# sudo apt-get purge audience
# sudo apt-get purge maya-calendar #calendar
sudo apt-get autoremove
sudo apt-get autoclean

# Properties Commons (to install elementary tweaks
sudo apt-get install software-properties-common
# Terminator
sudo apt-get install terminator
# Install File Compression Libs
sudo apt-get install rar unrar zip unzip p7zip-full p7zip-rar
# Git
sudo apt-get install git
# Htop
sudo apt-get install htop
# Tree
sudo apt-get install tree
# GParted
sudo apt-get install gparted
# VLC
sudo apt-get install vlc browser-plugin-vlc
# FireFox
sudo apt-get install firefox
# InkScape
sudo apt-get install inkscape
# OpenSSH
sudo apt install openssh-server
# GUI OpenVPN
sudo apt-get install network-manager-openvpn
sudo restart network-manager
# WPS
wget http://kdl.cc.ksosoft.com/wps-community/download/a21/wps-office_10.1.0.5672~a21_amd64.deb
sudo dpkg -i wps-office_10.1.0.5672~a21_amd64.deb
# Gdebi
sudo apt-get install gdebi

# MS Font
sudo apt-get install ttf-mscorefonts-installer
# Screenshot-Tool
sudo apt remove screenshot-tool
sudo add-apt-repository ppa:shutter/ppa
sudo apt-get update
sudo apt-get install -y shutter

# Dropbox (specific installation for elementary Loki)
git clone https://github.com/zant95/elementary-dropbox /tmp/elementary-dropbox
bash /tmp/elementary-dropbox/install.sh

# Elementary Tweak
# 1. adding repository
sudo add-apt-repository ppa:philip.scott/elementary-tweaks
# 2. updating apt-get
sudo apt-get update 
# 3. installing tweaks
sudo apt-get install elementary-tweaks

# Google Chrome
# 1. downloading last stable package
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
# 2. installing package
sudo dpkg -i google-chrome-stable_current_amd64.deb
# 3. fixing broken dependencies
sudo apt-get install -f
# 4. Enable maximize and minimize button on Google Chrome
gconftool-2 --set /apps/metacity/general/button_layout --type string ":minimize:maximize:close"

# JEnv
# 1. Downloading and installing package
curl -L -s get.jenv.io | bash
# 2. updating bash
source ~/.jenv/bin/jenv-init.sh
# 3. update jenv local repository
jenv selfupdate

#Install Spotify
# 1. Add the Spotify repository signing key to be able to verify downloaded packages
#sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886
# 2. Add the Spotify repository
#echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
# 3. Update list of available packages
#sudo apt-get update
# 4. Install Spotify
#sudo apt-get install spotify-client


# Atom
wget https://github.com/atom/atom/releases/download/v1.17.2/atom-amd64.deb
sudo apt install ./atom-*.deb
rm atom-*.deb # free up space

# installing docker
sudo rm /var/lib/apt/lists/* -vf
sudo apt-get update
curl -fsSl https://get.docker.com/ | sh

# VirtualBox
# 1. downloading package
# wget http://download.virtualbox.org/virtualbox/5.1.6/VirtualBox-5.1.6-110634-Linux_amd64.run
# 2. installing package into /opt
# sudo sh VirtualBox-5.1.6-110634-Linux_amd64.run
# 3. downloading extension pack
# wget http://download.virtualbox.org/virtualbox/5.1.6/Oracle_VM_VirtualBox_Extension_Pack-5.1.6-110634.vbox-extpack
# 4. install extension pack
# sudo VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.1.6-110634.vbox-extpack
# 5. listing installed extension packs
# sudo VBoxManage list extpacks

# SmartGIT
# 1. downloading package
wget http://www.syntevo.com/smartgit/download?file=smartgit/smartgit-8_0_1.deb
# 2. installing package
sudo dpkg -i smartgit-8_0_1.deb
# 3. fixing broken dependencies
sudo apt-get install -f

# PSensor
sudo apt-get install lm-sensors hddtemp
sudo sensors-detect
sudo apt-get install psensor


# Reduce overheating and improve battery life
# 1. adding repository
sudo add-apt-repository ppa:linrunner/tlp
# 2. updating apt-get
sudo apt-get update
# 3. installing package
sudo apt-get install tlp tlp-rdw
# 4. starting application
sudo tlp start
# 5. activate tlr in thinkpads
sudo apt-get install tp-smapi-dkms acpi-call-dkms
# 6. Install Linux Thermal Daemon, alongsite tlp
sudo apt-get install thermald

# Remove Guest Session
sudo sh -c "echo 'allow-guest=false' >> /usr/share/lightdm/lightdm.conf.d/40-pantheon-greeter.conf"

# exit super user
exit